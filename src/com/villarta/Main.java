package com.villarta;

public class Main {

    public static void main(String[] args) {

        // Mini-activity.
        Human person1 = new Human();
        person1.setName("Ian");

        Human person2 = new Human("Ian", 23,'M');


        Adult adult1 = new Adult("Juan", 35, 'M', "Lawyer");

        System.out.println(adult1.talk());
        System.out.println(adult1.getOccupation());

        Child child1 = new Child("John", 15,'M', "Computer Science");

        System.out.println(child1.talk());
        System.out.println(child1.getEducation());






}
}
