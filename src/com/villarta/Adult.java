package com.villarta;

public class Adult extends Human{

    private String occupation;

    //empty constructor
    public Adult() {}

    //parameterized constructor
    public Adult(String name, int age, char gender, String newOccupation){
        super(name, age, gender);
        this.occupation = newOccupation;

    }
    public String getOccupation(){
        return this.occupation;
    }

    public void setOccupation(String newOccupation){
        this.occupation = newOccupation;
    }

    //Customized talk method
    public String talk() {
        return "Hello, I'm " + getName() + "! And I'm an adult";
    }

}
