package com.villarta;

public class Child extends Human{

    private String education;

    public Child(){}

    public Child(String name, int age, char gender, String newEducation){
        super(name, age, gender);
        this.education = newEducation;

    }
    public String getEducation(){
        return this.education;
    }

    public void setEducation(String newEducation){
        this.education = newEducation;
    }

}
