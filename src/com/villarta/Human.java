package com.villarta;

public class Human {
    private String name;
    private int age;
    private char gender;

    public Human(){}

    public Human(String newName, int newAge, char newGender){
        this.name = newName;
        this.age = newAge;
        this.gender = newGender;
    }

    public String getName(){
        return this.name;
    }
    public int getAge(){
        return this.age;
    }
    public char getGender(){
        return this.gender;
    }

    public void setName(String newName){
        this.name = newName;
    }

    public void setAge(int newAge){
        this.age = newAge;
    }
    public void setGender(char newGender){
        this.gender = newGender;
    }

    public String talk(){
        return "My name is " + getName();
    }

}
